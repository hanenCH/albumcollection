const mongoose = require("mongoose");
const express = require("express");
const bodyParser = require("body-parser");
const logger = require("morgan");
const Albums = require("./albums");
const Photos = require("./photos");

const API_PORT = 3001;
const app = express();
const router = express.Router();

// this is our MongoDB database
const dbRoute = "mongodb://hanenCH:Ch7384@ds123224.mlab.com:23224/albumsdb";//"mongodb://127.0.0.1/albumsdb"; // used for test

// Get Mongoose to use the global promise library
mongoose.Promise = global.Promise;

// connects our back end code with the database
mongoose.connect(
  dbRoute,
  { useNewUrlParser: true }
);

let db = mongoose.connection;

db.once("open", () => console.log("connected to the database"));

// checks if connection with the database is successful
db.on("error", console.error.bind(console, "MongoDB connection error:"));

// (optional) only made for logging and
// bodyParser, parses the request body to be a readable json format
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(logger("dev"));


// this is our get method for Album documents
// this method fetches all available album in our database
app.get("/api/getAlbums", (req, res) => {
  Albums.getAlbums(function(err, albums){
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true, albums: albums });
  });
});


// this is our get method for Photo documents
// this method fetches all available photo in a given album 
app.get("/api/getPhotos", (req, res) => {
	var  Id  = req.query.albumId;
  Photos.find({albumId: Id}, (err, photos) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true, photos: photos  });
  });
});


// launch our backend into a port
app.listen(API_PORT, () => console.log(`LISTENING ON PORT ${API_PORT}`));