const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const AlbumSchema = new Schema(
  {
    id: Number,
    title: String,
    coverphoto: String,
    description: String
        
  }

);

var Albums = module.exports = mongoose.model("Albums", AlbumSchema);

// this method fetches all available album in our database
module.exports.getAlbums = function(callback, limit){
  Albums.find(callback).limit(limit);
}


