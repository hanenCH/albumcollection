const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const PhotoSchema = new Schema(
  {
     albumId : Number,
     id : Number,
     title : String,
     url : String,
     thumbnailUrl :String
  }

);



var Photos = module.exports = mongoose.model("Photos", PhotoSchema);

module.exports.getPhotos = function(callback, limit){
  Photos.find(callback).limit(limit);
}
