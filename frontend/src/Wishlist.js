import React, { Component } from "react";
import {  Card, CardImg, CardBody,
  CardTitle, CardSubtitle, Container, Row, Col } from 'reactstrap';

class Wishlist extends Component {

  state = {
    albums: []
  };

  componentWillMount() { 
    this.getWishList();    
  }

  // Get the data (wishList) from the local storage 
  getWishList = () => {
    if (localStorage.getItem("wishList")!=null){
    this.setState({albums: JSON.parse(localStorage.getItem("wishList"))});  
  }  
  };


  render() {
     const { albums } = this.state;
    return (       
      <div>
        <Container>
        <Row>
          {albums.length <= 0 ? "THE WISH LIST IS EMPTY" : albums.map(dat => (
            dat==null ? "" : 
            <Col xs="4">
              <Card>
                <CardImg top width="100%" src={dat.url} alt="" />
                <CardBody>
                  <CardTitle>{dat.title}</CardTitle>
                  <CardSubtitle></CardSubtitle>
                </CardBody>
              </Card>
            </Col>
          ))}
        </Row>
        </Container> 
      </div>
    );
  }
}

export default Wishlist;