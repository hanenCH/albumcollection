
import React, { Component } from "react";
import NavMenu from './NavMenu';
import Album from './Album';
import Home from './Home';
import Contact from './Contact'
import Wishlist from './Wishlist';
import { BrowserRouter, Route, Switch } from 'react-router-dom';



class App extends Component {


  render() {   
    return (   
      <div>
        <BrowserRouter>
        <div>
          <NavMenu/>
          <Switch>
            <Route path="/" component={Home} exact />
            <Route path="/Album/:albumId" component={Album} /> {/* Used to Initialize the link to the photos of the first album*/}
            <Route path="/Wishlist" component={Wishlist} />
            <Route path="/Contact" component={Contact} />
          </Switch>
        </div>
      </BrowserRouter>
        <br />
        <hr />
        <footer class="footer">
          <div class="container">
          <center>
            <span class="text-muted">
            ALBUMS COLLECTION: A simple albums application <br />
            Used technologies: React JS, Mangodb, MLab, Express, React-Router, Reactstrap, npm, git, ...
            </span>
            </center>
          </div>
        </footer>
      </div>    
    );
  }
}
export default App;