import React, { Component } from "react";
import { NavLink } from 'react-router-dom';
import {  Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, Container, Row, Col } from 'reactstrap';

class Home extends Component {
  state = {
    albums: [],
    id: 0
  };

  componentDidMount() {
    this.getAlbumsFromDb();
  }

  // Select albums from the DB (API)
  getAlbumsFromDb = () => {
    fetch("/api/getAlbums")
      .then(albums => albums.json())
      .then(res => this.setState({ albums: res.albums }));
  };


  render() {
     const { albums } = this.state;
    return (       
      <div>
        <Container>
          <Row>
            {albums.length <= 0 ? "NO ALBUM ENTRIES YET" : albums.map(dat => (
              <Col xs="6">
                <Card>
                  <CardImg top width="100%" src={dat.coverphoto} alt="" />
                  <CardBody>
                    <CardTitle>{dat.title}</CardTitle>
                    <CardSubtitle></CardSubtitle>
                    <CardText>
                      {dat.description}
                      <NavLink to={"/Album/"+dat.id}> Show</NavLink>
                    </CardText>
                  </CardBody>
                </Card>
              </Col>
            ))}
          </Row>
        </Container> 
        
        </div>
    );
  }
}

export default Home;