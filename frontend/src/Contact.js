import React from "react";



function Contact({ match }) {
  return (
  	<div>
	    <div class="jumbotron">
		  <h1 class="display-3">ALBUMS COLLECTION</h1>
		  <p class="lead"></p>
		  <hr class="my-4" />
		  <p>
		  Author : Hanen Chihi <br />
		  Software engineer  <br />
		  Doctor of computer science
		  </p>
		  <p class="lead">
		    <a class="btn btn-primary btn-lg" href="https://www.linkedin.com/in/hanen-chihi" role="button">More details</a>
		  </p>
		</div>
    </div>
  );
}
export default Contact;
