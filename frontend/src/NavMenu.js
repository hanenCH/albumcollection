import React, {Component} from 'react';
import { Link } from 'react-router-dom'; 
import {  Nav, NavItem, NavLink } from 'reactstrap';




class NavMenu extends Component {
render() {
        return (
          <div>
            <Nav>
                <NavItem>
                  <NavLink><Link to="/">HOME</Link></NavLink>
                </NavItem>
                <NavItem>
                  <NavLink><Link to="/Album/1">ALBUM</Link></NavLink>
                </NavItem>
                <NavItem>
                  <NavLink><Link to="/Wishlist">WISHLIST</Link></NavLink>
                </NavItem>
                <NavItem>
                  <NavLink><Link to="/Contact">CONTACT</Link></NavLink>
                </NavItem>
            </Nav>
            <hr />
          </div>
        
        );
      }

      };

export default NavMenu;