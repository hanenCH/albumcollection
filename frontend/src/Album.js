import React, { Component } from 'react';
import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption,
  Button
} from 'reactstrap';


class Album extends Component {
  
   constructor(props) {
    super(props);
    //  localStorage.removeItem("wishList");   //=> Remove the Item from the local storage
    this.state = { 
      activeIndex: 0, 
      photos: [],
      albumId: props.match.params.albumId}; 
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
  }
  


// Add the carousel active item (Selected element) to the local storage
addToWhish(){
  var selectedPhoto = this.state.activeIndex;
  var wishList = [];

  if (localStorage.getItem("wishList")==null){
    wishList.push(this.state.photos[selectedPhoto]);
    localStorage.setItem("wishList", JSON.stringify(wishList));
  } else {
    wishList = JSON.parse(localStorage.getItem("wishList"));
    wishList.push(this.state.photos[selectedPhoto]);
    localStorage.setItem("wishList", JSON.stringify(wishList));    
  }        
}

componentDidMount() { 
    this.getPhotosFromDb();    
  }

 // Select photos of an album from the DB (API)
  getPhotosFromDb = () => {
    fetch("/api/getPhotos?albumId="+this.state.albumId)
      .then(photos => photos.json())
      .then(res => this.setState({ photos: res.photos }));
  };
  
  onExiting() {
    this.animating = true;
  }

  onExited() {
    this.animating = false;
  }

  next() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === this.state.photos.length - 1 ? 0 : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  }

  previous() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === 0 ? this.state.photos.length - 1 : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  }

  goToIndex(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex: newIndex });
  }

  render() {
    const { activeIndex } = this.state;
    const { photos } = this.state;
    const slides = photos.map((item) => {    
      return (
        <CarouselItem
          onExiting={this.onExiting}
          onExited={this.onExited}
          key={item.url}
        >
          <img src={item.url} width="100%" alt="" />
          <CarouselCaption captionText="" captionHeader={item.title} />
          <h3> </h3>
        </CarouselItem>
      );
    });
  
  return (
    <div>
      <center>
        <Button onClick={this.addToWhish.bind(this)} color="secondary">Add To Wish List</Button>
      </center>
      <Carousel
          activeIndex={activeIndex}
          next={this.next}
          previous={this.previous}
        >
          <CarouselIndicators items={photos} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
          {slides}
          <CarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous} />
          <CarouselControl direction="next" directionText="Next" onClickHandler={this.next} />
        </Carousel>
        
        <div>
      </div>
    </div>
    );
  }
}
export default Album;